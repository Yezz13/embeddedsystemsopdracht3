/* A simple server in the internet domain using TCP
   The port number is passed as an argument 
   This version runs forever, forking off a separate 
   process for each connection
*/
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h> 
#include <sys/socket.h>
#include <netinet/in.h>

void dostuff(int); /* function prototype */
void error(const char *msg)
{
    perror(msg);
    exit(1);
}

int main(int argc, char *argv[])
{
     int sockfd, newsockfd, portno, pid;
     socklen_t clilen;
     struct sockaddr_in serv_addr, cli_addr;

     if (argc < 2) {
         fprintf(stderr,"ERROR, no port provided\n");
         exit(1);
     }
     sockfd = socket(AF_INET, SOCK_STREAM, 0);
     if (sockfd < 0) 
        error("ERROR opening socket");
     bzero((char *) &serv_addr, sizeof(serv_addr));
     portno = atoi(argv[1]);
     serv_addr.sin_family = AF_INET;
     serv_addr.sin_addr.s_addr = INADDR_ANY;
     serv_addr.sin_port = htons(portno);
     if (bind(sockfd, (struct sockaddr *) &serv_addr,
              sizeof(serv_addr)) < 0) 
              error("ERROR on binding");
     listen(sockfd,5);
     clilen = sizeof(cli_addr);
     while (1) {
         newsockfd = accept(sockfd, 
               (struct sockaddr *) &cli_addr, &clilen);
         if (newsockfd < 0) 
             error("ERROR on accept");
         pid = fork();
         if (pid < 0)
             error("ERROR on fork");
         if (pid == 0)  {
             close(sockfd);
             dostuff(newsockfd);
             exit(0);
         }
         else close(newsockfd);
     } /* end of while */
     close(sockfd);
     return 0; /* we never get here */
}

/******** DOSTUFF() *********************
 There is a separate instance of this function 
 for each connection.  It handles all communication
 once a connnection has been established.
 *****************************************/
void dostuff (int sock)
{
    ssize_t len;
    char buffer[BUFSIZ];
    int file_size, n;
    FILE *received_file;
    int remain_data = 0;
    char FILENAME[256];
    char* stringVariable;
    char* stringArray[2];
    int i = 0;

    /* Get the file name that the client will send */
    bzero(buffer,256);
    n = read(sock,buffer,255);

    /* Split the file name to get the name and the extension apart */
    stringVariable = strtok (buffer,".");
    while (stringVariable != NULL)
    {
        stringArray[i] = stringVariable;
        stringVariable = strtok (NULL, "."); 
        i++;  
    }

    /* get the time of today */
    time_t current_time;
    char* c_time_string;

    /* Obtain current time. */
    current_time = time(NULL);

    if (current_time == ((time_t)-1))
    {
        (void) fprintf(stderr, "Failure to obtain the current time.\n");
        exit(EXIT_FAILURE);
    }

    /* Convert to local time format. */
    c_time_string = ctime(&current_time);
    if (c_time_string == NULL)
    {
        (void) fprintf(stderr, "Failure to convert the current time.\n");
        exit(EXIT_FAILURE);
    }
    strtok(c_time_string, "\r\n");

    /* Put everything together for the file name */
    bzero(FILENAME,256);
    strcpy(FILENAME, stringArray[0]);
    strcat(FILENAME, "_");
    strcat(FILENAME, c_time_string);
    strcat(FILENAME, ".");
    strcat(FILENAME, stringArray[1]);

    /* Show an example how the file will be named */
    printf("%s\r\n",FILENAME);

    /* Make a file with the file name that is created */
    received_file = fopen(FILENAME, "w");
    if (received_file == NULL)
    {
            fprintf(stderr, "Failed to open file");

            exit(EXIT_FAILURE);
    }
    n = write(sock,"Start sending",18);

    /* Receiving file size */
    recv(sock, buffer, BUFSIZ, 0);
    file_size = atoi(buffer); 
    remain_data = file_size;

    while (((len = recv(sock, buffer, BUFSIZ, 0)) > 0) && (remain_data > 0))
    {
            fwrite(buffer, sizeof(char), len, received_file);
            remain_data -= len;
            fprintf(stdout, "Receive %d bytes and we hope :- %d bytes\n", len, remain_data);
            if(remain_data == 0 || remain_data <= 0){
                break;
            }
    }  
    printf("Got all the data\r\n");  
    fclose(received_file);

    n = write(sock,"I got your message",18);
    if (n < 0){
        error("ERROR writing to socket");
    }
}
